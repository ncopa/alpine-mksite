<div class="pure-g">

    <div class="pure-u-1">
        <div class="l-box">
            <h1>Cloud Images</h1>
            <p><i>
                Currently only AWS EC2 is supported. Other cloud providers will
                be added over time.
            </i></p>
            <p>
                The login user for the images is <b>alpine</b>. SSH keys for
                this user will be installed from the instance metadata service.
            </p>
        </div>
    </div>

    {{#cloud/releases.clouds.aws}}
    <div class="pure-u-1">
        <div class="cloud">
            <h2>{{version}}</h2>
            <table class="pure-table pure-table-bordered">
                <thead>
                    <tr>
                        <th>Provider</th>
                        <th>Region</th>
                        <th>Architecture</th>
                        <th>Released</th>
                        <th>EOL Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {{#releases}}
                {{#artifacts}}
                    <tr>
                        <td>AWS EC2</td>
                        <td><a href="https://{{region}}.console.aws.amazon.com/ec2/home#Images:visibility=public-images;imageId={{image_id}}">{{region}}</a></td>
                        <td>{{arch}}</td>
                        <td>{{creation_date}}</td>
                        <td>{{end_of_life}}</td>
                        <td>
                            <a href="https://{{region}}.console.aws.amazon.com/ec2/home#launchAmi={{image_id}}" class="green-button">
                                <i class="fa fa-download"></i>&nbsp;Launch
                            </a>
                        </td>
                    </tr>
                {{/artifacts}}
                {{/releases}}
                </tbody>
            </table>
        </div>
    </div>
    {{/cloud/releases.clouds.aws}}

</div> <!-- end download -->
