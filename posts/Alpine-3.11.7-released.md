---
title: 'Alpine 3.11.7 released'
date: 2020-12-16
---

Alpine Linux 3.11.7 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.11.7 of its Alpine Linux operating system.

This includes an important security fix for openssl (CVE-2020-1971).

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.11.7).

Git Shortlog
------------

<pre>
6543 (2):
      community/gitea: upgrade to v1.10.6
      community/go: upgrade to 1.13.13

Alex Denes (1):
      main/postgresql: security upgrade to 12.5

Andy Postnikov (11):
      community/php7: security upgrade to 7.3.18 CVE-2019-11048
      community/drupal7: security upgrade to 7.70
      community/drupal7: security upgrade to 7.72 CVE-2020-13663
      community/drupal7: security upgrade to 7.73 CVE-2020-13666
      community/php7-pecl-timezonedb: upgrade to 2020.1
      community/php7-pecl-timezonedb: fix license and improve
      community/php7-pecl-timezonedb: upgrade to 2020.2
      community/php7-pecl-timezonedb: upgrade to 2020.3
      community/php7-pecl-timezonedb: upgrade to 2020.4
      community/drupal7: security upgrade to 7.74 - CVE-2020-13671
      community/drupal7: security upgrade to 7.75

Ariadne Conill (2):
      main/musl: security fix for CVE-2020-28928
      main/tzdata: switch to fat format

Carlo Landmeter (1):
      community/salt: upgrade to 2019.2.4

Daniel Néri (10):
      main/xen: Remove dependency on syslinux
      [3.11] main/xen: backport upgrade to 4.13.1
      main/ruby: Correct wrong CVE number in secfixes for version 2.6.6-r0
      main/xen: security fixes for XSA-317, XSA-319, XSA-321, XSA-327 and XSA-328
      [3.11] main/alpine-conf: avoid unwanted syslinux for setup-disk on mounted root
      main/xen: security fix for CVE-2020-14364/XSA-335
      main/xen: security fixes for XSA-333, XSA-334, XSA-336, XSA-337, XSA-338, XSA-339, XSA-340, XSA-342, XSA-343 and XSA-344
      main/xen: security fix for XSA-351
      main/xen: security fix for XSA-355
      main/xen: CVE-2020-28368 assigned to XSA-351

Francesco Colista (2):
      main/libvirt: security fix for CVE-2020-12430
      main/libvirt: security fix for CVE-2019-20485

Henrik Riomar (9):
      community/intel-ucode: upgrade to 20200609
      main/xen: fix XSA-320
      community/intel-ucode: upgrade to 20200616
      main/fail2ban: fix logging after rotate
      main/rsyslog: drop boot.log
      main/rsyslog: rsyslog logrotate should not touch messages
      main/xen: security upgrade to 4.13.2
      community/intel-ucode: security upgrade to 20201110
      community/intel-ucode: security upgrade to 20201112

J0WI (32):
      main/tzdata: upgrade to 2020a
      community/python2-tkinter: security upgrade to 2.7.17
      community/python2-tkinter: security upgrade to 2.7.18
      main/libxml2: fix CVE-2019-20388
      community/ceph: security upgrade to 14.2.7
      community/ceph: security upgrade to 14.2.9
      main/ldb: upgrade to 2.0.10
      main/samba: security upgrade to 4.11.8
      main/mariadb: security upgrade to 10.4.13
      main/perl-mozilla-ca: upgrade to 20200520
      community/mumble: security upgrade to 1.3.1
      community/ffmpeg: security upgrade to 4.2.4
      community/firefox-esr: security upgrade to 68.10.0
      community/firefox-esr: security upgrade to 68.11.0
      main/spamassassin: security upgrade to 3.4.4
      community/openjdk7: security upgrade to 7.231.2.6.19
      community/openjdk7: security upgrade to 7.241.2.6.20
      community/openjdk7: security upgrade to 7.251.2.6.21
      community/openjdk7: security upgrade to 7.261.2.6.22
      community/openjdk8: security upgrade to 8.252.09
      main/gnutls: security upgrade to 3.6.15
      community/pdns: upgrade to 4.2.2
      community/pdns: security upgrade to 4.2.3
      main/ansible: upgrade to 2.9.9
      main/ansible: security update to 2.9.13
      community/apache-ant: security upgrade to 1.10.9
      main/mariadb: security upgrade to 10.4.15
      main/tzdata: upgrade to 2020b
      main/ldb: upgrade to 2.0.12
      main/samba: security upgrade to 4.11.14
      main/samba: security upgrade to 4.11.16
      main/openssl: security upgrade to 1.1.1i

Jake Buchholz (1):
      [3.11] community/containerd: update to 1.3.9

Jakub Jirutka (3):
      community/rtorrent: rebuild
      main/postgresql: security upgrade to 12.4
      main/mbedtls: upgrade to 2.16.9

Jesse Olson (1):
      community/prometheus: correct init.d variables

Justin Berthault (1):
      main/clamav: security upgrade to 0.102.4

Kaarle Ritvanen (1):
      main/apache2: security upgrade to 2.4.46

Keith Maxwell (1):
      main/py3-httplib2: fix CWE-93

Kevin Daudt (4):
      community/go: security upgrade to 1.13.10 (CVE-2020-7919)
      community/various: rebuild go packages for CVE-2020-7919
      community/umoci: add chmod-clean option
      community/zabbix: upgrade to zabbix 4.4.9

Leo (70):
      community/chromium: explicitly call python2 instead of python
      main/re2c: fix CVE-2020-11958
      main/libxml2: modernize
      community/ceph: remove stale boost-1.70 patch
      main/openldap: fix CVE-2020-12243
      main/libexif: security upgrade to 0.6.22
      main/unbound: fix CVE-2020-12662 and CVE-2020-12663
      main/bind: security upgrade to 9.14.12
      community/knot-resolver: fix CVE-2020-12667
      community/wireshark: security upgrade to 3.0.11
      community/pdns-recursor: security upgrade to 4.2.2
      main/iproute2: add missing secfixes info
      community/apache-ant: security upgrade to 1.10.8
      main/clamav: security upgrade to 0.102.3
      community/vlc: security upgrade to 3.0.9.2
      main/mbedtls: security upgrade to 2.16.6
      main/python3: add missing secfixes info
      main/json-c: fix CVE-2020-12762
      main/dbus: fix CVE-2020-12049
      main/gnutls: add corresponding GNUTLS-SA to CVE-2020-13777
      main/axel: fix CVE-2020-13614
      main/hostapd: fix CVE-2020-12695
      main/perl: fix typo in include for BZIP2
      main/libjpeg-turbo: fix CVE-2020-13790
      main/ngircd: fix CVE-2020-14148
      main/python3: fix CVE-2020-14422
      main/hylafaxplus: fix CVE-2020-15396 and CVE-2020-15397
      main/xorg-server: fix CVE-2020-14347
      main/patch: add missing CVE to secfixes
      main/zeromq: security upgrade to 4.3.3
      main/libssh: fix CVE-2020-16135
      main/curl: fix CVE-2020-8169 and CVE-2020-8177
      main/libxml2: fix CVE-2020-24977
      main/openjpeg: fix CVE-2019-12973 and CVE-2020-15389
      main/perl-dbi: security upgrade to 1.643
      main/cryptsetup: fix CVE-2020-14382
      community/wireshark: security upgrade to 3.0.14
      main/ansible: upgrade to 2.9.11
      main/mbedtls: security upgrade to 2.16.8
      community/apache-ant: fix checksum
      main/oniguruma: fix CVE-2020-26159
      main/ansible: upgrade to 2.9.14
      main/tzdata: upgrade to 2020c
      main/freetype: fix CVE-2020-15999
      main/xorg-server: fix various CVEs
      main/perl-datetime-timezone: upgrade to 2.41
      main/perl-datetime-timezone: upgrade to 2.42
      main/perl-datetime-timezone: upgrade to 2.43
      main/open-iscsi: upgrade to 2.1.2
      main/tmux: fix CVE-2020-27347
      main/krb5: security upgrade to 1.17.2
      main/tcpdump: fix CVE-2020-8037
      main/curl: fix CVE-2020-8231
      main/redis: fix CVE-2015-8080
      main/bluez: fix CVE-2020-27153
      main/bluez: fix CVE-2020-27153
      main/openldap: fix a few CVEs
      main/openldap: use local patch for CVE-2020-12243
      main/pcre: fix CVE-2020-14155
      main/cups: fix CVE-2019-8842 and CVE-2020-3898
      main/dovecot: fix CVE-2020-12673 and CVE-2020-12674
      main/mariadb-connector-c: fix CVE-2020-13249
      main/squid: add missing secfixes info
      main/squid: fix typo in CVE identifier
      main/nrpe: fix CVE-2020-6581 and CVE-2020-6582
      main/py3-django: fix CVE-2020-24583 and CVE-2020-24584
      main/curl: fix CVE-2020-8285 and CVE-2020-8286
      main/p11-kit: fix CVE-2020-29361 CVE-2020-29362 CVE-2020-29363
      main/nrpe: revert fixes for CVE-2020-6581 and CVE-2020-6582
      main/nrpe: rebuild after revert

Leonardo Arena (12):
      community/nextcloud: upgrade to 17.0.6
      main/sqlite: security fix (CVE-2020-11655)
      main/dovecot: security upgrade to 2.3.10.1
      community/exim: security fix (CVE-2020-12783)
      main/jbig2dec: security fix (CVE-2020-12268)
      main/jbig2dec: update checksums
      community/nextcloud: upgrade to 17.0.7
      main/smokeping: needs ttf-dejavu
      community/nextcloud: upgrade to 17.0.9
      community/zabbix: upgrade to 4.4.10
      community/nextcloud: upgrade to 17.0.10
      community/zabbix: rundir is needed for control socket

Michael Kirsch (1):
      main/knock: upgrade to 0.8.1

Milan P. Stanić (3):
      community/firefox-esr: security upgrade to 68.8.0
      main/postfix: upgrade to 3.4.12
      main/perl-datetime-timezone: upgrade to 2.39

Natanael Copa (61):
      community/chromium: security upgrade to 80.0.3987.149
      community/chromium: upgrade to 81.0.4044.113
      Revert "main/ncurses: fix missing vtXXX terminfo in ncurses-terminfo-base"
      main/libxml2: store patch in aports tree
      main/sprunge: use https when possible
      main/abuild: backport fixes for crosscompile
      main/ca-certificates: remove expired certificate
      main/ca-certificates: use source package from gitlab
      main/gnutls: security upgrade to 3.6.14 (CVE-2020-13777)
      main/perl: security upgrade to 5.30.3 (CVE-2020-10543,CVE-2020-10878,CVE-2020-12723)
      main/gcc: security upgrade to 9.3.0 (CVE-2019-15847)
      main/busybox: add secfixes comment for CVE-2018-1000500
      main/ansible: remove duplicate in secfixes comment
      community/exim: remove dup CVE-2018-6789 in secfixes comment
      main/putty: upgrade to 0.74 (CVE-2020-14002)
      main/ghostscript: clean up duplicate secfixes comment
      community/firefox: remove duplicate CVE in secfixes comment
      main/lame: remove duplicates in secfixes comment
      main/sdl: remove duplicate CVE in secfixes comment
      community/wireshark: fix secfixes comment
      main/samba: remove duplicate CVE in secfixes comment
      main/sqlite: fix secfixes comment
      main/hostapd: remove duplicate CVE in secfixes comment
      main/libsndfile: remove dulicate CVE secfixes comment
      main/libvorbis: remove duplicate CVE in secfixes comment
      main/wpa_supplicant: remove CVE dupes in secfixes comment
      main/busybox: fix duplicate CVE in secfixes comment
      main/rdesktop: fix duplicate CVEs in secfixes comment
      community/tor: fix duplicate CVE in secfixes comment
      main/linux-lts: upgrade to 5.4.72
      community/jool-modules-lts: rebuild against kernel 5.4.72-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.72-r0
      community/wireguard-lts: rebuild against kernel 5.4.72-r0
      main/drbd-lts: rebuild against kernel 5.4.72-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.72-r0
      main/zfs-lts: rebuild against kernel 5.4.72-r0
      main/linux-rpi: upgrade to 5.4.72
      community/jool-modules-rpi: rebuild against kernel 5.4.72-r0
      community/wireguard-rpi: rebuild against kernel 5.4.72-r0
      main/squid: security upgrade to 4.13
      main/linux-lts: upgrade to 5.4.83
      community/jool-modules-lts: rebuild against kernel 5.4.83-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.83-r0
      community/wireguard-lts: upgrade to 1.0.20201112 / 5.4.83-r0
      main/drbd-lts: rebuild against kernel 5.4.83-r0
      main/xtables-adons-lts: rebuild against kernel 5.4.83-r0
      main/zfs-lts: rebuild against kernel 5.4.83-r0
      main/linux-rpi: upgrade to 5.4.83
      community/jool-modules-rpi: rebuild against kernel 5.4.83-r0
      community/wireguard-rpi: upgrade to 1.0.20201112 / 5.4.83-r0
      main/linux-rpi: upgrade to 5.4.84
      community/jool-modules-rpi: rebuild against kernel 5.4.84-r0
      community/wireguard-rpi: rebuild against kernel 5.4.84-r0
      main/linux-lts: upgrade to 5.4.84
      community/jool-modules-lts: rebuild against kernel 5.4.84-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.84-r0
      community/wireguard-lts: rebuild against kernel 5.4.84-r0
      main/drbd-lts: rebuild against kernel 5.4.84-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.84-r0
      main/zfs-lts: rebuild against kernel 5.4.84-r0
      ===== release 3.11.7 =====

Rasmus Thomsen (14):
      main/vala: upgrade to 0.46.9
      community/mutter: upgrade to 3.34.6
      community/gnome-desktop: upgrade to 3.34.6
      community/gnome-control-center: upgrade to 3.34.6
      community/webkit2gtk: upgrade to 2.28.2
      main/python2: security upgrade to 2.7.18
      community/rpm: build without broken plugin support
      community/cheese: add missing dep on gsettings-desktop-schemas
      main/vala: upgrade to 0.46.10
      community/gnome-photos: upgrade to 3.34.1
      main/vala: upgrade to 0.46.11
      main/vala: upgrade to 0.46.12
      community/firefox-esr: disable, fails to build
      main/vala: upgrade to 0.46.13

Simon Frankenberger (2):
      main/nghttp2: fix CVE-2020-11080
      community/php7: upgrade to 7.3.22

Sora Morimoto (1):
      community/opam: upgrade to 2.0.7

Stefan Reiff (1):
      main/samba: upgrade to 4.11.9

Sören Tempel (1):
      community/firefox-esr: upgrade to 68.9.0

TBK (2):
      main/ntfs-3g: patch CVE-2019-9755
      community/java-gcj-compat: update gccpkgrel to match gcc6-java

Yonggang Luo (1):
      community/chromium: install swiftshader

aptalca (1):
      [3.11] main/libmaxminddb: fix database retrieval Backport of https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/7853  - Allow MaxMind license key input required for downloads  - Update db retrieval (new endpoint with license key    and new compressed file structure)

iggy (1):
      community/ceph: upgrade to 14.2.8

prspkt (4):
      main/dropbear: backport security fixes
      main/libx11: security upgrade to 1.6.10
      main/chrony: security upgrade to 3.5.1
      main/libx11: security upgrade to 1.6.12

</pre>
