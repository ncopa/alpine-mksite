---
title: 'Alpine 3.12.1 released'
date: 2020-10-21
---

Alpine Linux 3.12.1 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.12.1 of its Alpine Linux operating system.

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.12.1).

Git Shortlog
------------

<pre>
6543 (2):
      community/gitea: upgrade to v1.11.8
      community/go: upgrade to 1.13.14

Andy Postnikov (30):
      community/php7-pecl-xdebug: upgrade to 2.9.6
      community/php7: upgrade to 7.3.19
      community/drupal7: security upgrade to 7.72 CVE-2020-13663
      community/composer: upgrade to 1.10.8
      community/php7-pecl-redis: enable bundled lzf compression
      community/nodejs-current: security upgrade to 14.4.0
      community/php7: upgrade to 7.3.20
      community/composer: upgrade to 1.10.9
      community/php7-pecl-msgpack: upgrade to 2.1.1
      community/composer: upgrade to 1.10.10
      community/php7-pecl-igbinary: upgrade to 3.1.4
      community/php7: upgrade to 7.3.21
      community/php7: upgrade to 7.3.22
      community/php7-pecl-ast: upgrade to 1.0.9
      community/php7-brotli: upgrade to 0.11.1
      community/php7-pecl-event: upgrade to 2.5.7
      community/php7-pecl-oauth: upgrade to 2.0.6
      community/php7-pecl-mailparse: upgrade to 3.1.1
      community/php7-pecl-xdebug: upgrade to 2.9.7
      community/drupal7: security upgrade to 7.73 CVE-2020-13666
      community/php7-pecl-xdebug: upgrade to 2.9.8
      community/php7: security upgrade to 7.3.23
      community/php7-pecl-apcu: upgrade to 5.1.19
      community/php7-pecl-uuid: upgrade to 1.2.0
      community/php7-pecl-xhprof: upgrade to 2.2.2
      community/php7-pecl-igbinary: upgrade to 3.1.6
      community/php7-pecl-timezonedb: upgrade to 2020.2
      community/phpmyadmin: upgrade to 5.0.3
      community/phpmyadmin: upgrade to 5.0.4
      community/php7-pecl-timezonedb: upgrade to 2020.3

Antoine Fontaine (1):
      community/pulseaudio: install pulseaudio-bluez if bluez is installed

Ariadne Conill (3):
      main/mariadb-connector-c: fix include and libdir paths in mariadb_config
      main/postgresql: add icu-dev to postgresql-dev
      community/mu: disable on s390x

Bart Ribbers (8):
      community/phosh: backport patch to fix a phantom launcher
      community/kpeople: add missing dep on qt5-qtbase-sqlite
      {community,testing}/kde-applications: upgrade to 20.04.2
      community/elisa: add missing runtime dep on vlc
      community/ark: fix CVE-2020-16116
      community/qt5-qtbase: fix CVE-2020-17507
      community/ark: fix CVE-2020-24654
      community/kdenlive: add missing runtime deps

Brian Davis (1):
      main/libxml2: fix CVE-2020-24977

Danct12 (1):
      community/squeekboard: use a better terminal keyboard layout

Daniel Néri (7):
      main/busybox: add hvc0 (Xen domain console) to /etc/securetty
      main/ruby: Correct wrong CVE number in secfixes for version 2.6.6-r0
      main/xen: security fixes for XSA-317, XSA-319, XSA-321, XSA-327 and XSA-328
      main/bind: security upgrade to 9.16.6
      community/qemu: security fix for CVE-2020-14364
      main/xen: security fix for CVE-2020-14364/XSA-335
      main/xen: security fixes for XSA-333, XSA-334, XSA-336, XSA-337, XSA-338, XSA-339, XSA-340, XSA-342, XSA-343 and XSA-344

Dennis Vestergaard Værum (1):
      main/busybox: increase max log line length from 512 to 2048

Dermot Bradley (1):
      linux-rpi: add common RPI RTC driver to kernel

Drew DeVault (1):
      community/py3-pygit2: update dependencies

Francesco Colista (11):
      community/graphicsmagick: security fix for CVE-2020-12672
      community/zabbix: fix creation of /run/zabbix for zabbix-agentd
      community/git-review: moved from testing
      community/monitoring-plugins: added bind-tools amond deps. Fixes #11712
      main/libvirt: upgrade to 6.5.0
      community/py3-libvirt: upgrade to 6.5.0
      community/jenkins: security upgrade to 2.245
      main/libvirt: security upgrade to 6.6.0. Fixes #11856
      main/libvirt: backport patches to fix deadlock issues with musl
      community/icingaweb2: security fix for CVE-2020-24368. Fixes #11897
      main/libvirt: security fix for CVE-2020-25637

Henrik Riomar (7):
      community/intel-ucode: upgrade to 20200609
      main/xen: fix XSA-320
      community/intel-ucode: upgrade to 20200616
      main/fail2ban: fix process name
      main/fail2ban: fix logging after rotate
      community/go: security upgrade to 1.13.15
      community/vault: security upgrade to 1.4.7

Holger Jaekel (2):
      community/gdal: upgrade to 3.1.1
      community/gdal: upgrade to 3.1.2

Isaiah Inuwa (1):
      main/unzip: Fix unsigned overflow patch

J0WI (30):
      community/mumble: security upgrade to 1.3.1
      community/vlc: security upgrade to 3.0.11
      community/pdns-recursor: security upgrade to 4.3.2 (CVE-2020-14196)
      community/firefox-esr: security upgrade to 68.10.0
      community/firefox: security upgrade to 78.0.1
      main/nss: upgrade to 3.54
      community/firefox-esr: security upgrade to 68.11.0
      community/imagemagick: upgrade to 7.0.10.18
      community/imagemagick: upgrade to 7.0.10.25
      community/imagemagick6: upgrade to 6.9.11.18
      community/imagemagick6: upgrade to 6.9.11.25
      community/openjdk7: security upgrade to 7.231.2.6.19
      community/openjdk7: security upgrade to 7.241.2.6.20
      community/openjdk7: security upgrade to 7.251.2.6.21
      community/openjdk7: security upgrade to 7.261.2.6.22
      community/openjdk8: security upgrade to 8.252.09
      main/samba: upgrade to 4.12.5
      main/samba: upgrade to 4.12.6
      main/gnutls: security upgrade to 3.6.15
      main/gnupg: security upgrade to 2.2.23
      community/mozjs68: security upgrade to 68.11.0
      community/mozjs68: security upgrade to 68.12.0
      community/mozjs68: add $pkgname to depends_dev
      community/firefox-esr: security upgrade to 78.3.0
      community/pdns: security upgrade to 4.2.3
      main/ansible: security update to 2.9.13
      community/apache-ant: security upgrade to 1.10.9
      main/samba: security upgrade to 4.12.7
      main/mariadb: security upgrade to 10.4.15
      main/tzdata: backport build fix from edge

Jake Buchholz (2):
      community/docker: upgrade to 19.03.11
      community/docker: upgrade to 19.03.12

Jakub Jirutka (13):
      main/pspg: upgrade to 3.1.2
      main/nodejs: fix renaming man pages
      main/nodejs: upgrade to 12.17.0
      main/postgresql: security upgrade to 12.4
      main/knot: upgrade to 2.9.6
      main/libuv: upgrade to 1.38.1
      main/libuv: fix CVE-2020-8252
      community/ruby-rspec-core: upgrade to 3.9.3
      main/pspg: upgrade to 3.1.4
      community/knot-resolver: upgrade to 5.1.3
      main/pspg: upgrade to 3.1.5
      main/nodejs: upgrade to 12.19.0
      Revert "main/nodejs: upgrade to 12.19.0"

John Boehr (1):
      main/lmdb: fix invalid pkgconfig

Justin Berthault (4):
      community/gnome-initial-setup: upgrade to 3.36.3
      community/gnome-builder: upgrade to 3.36.1
      community/coturn: security upgrade to 4.5.1.3
      main/clamav: security upgrade to 0.102.4

Kaarle Ritvanen (1):
      main/apache2: security upgrade to 2.4.46

Kevin Daudt (10):
      community/zabbix: upgrade to 5.0.1
      community/vault: security upgrade to 1.4.3 (CVE-2020-13223)
      community/firefox: add missing patch
      community/wireshark: remove conflict markers
      main/gnupg: don't error when gnupg group exists
      community/mlt: fix deadlock due to recursive mutex
      community/mlt: bump pkgrel
      community/zabbix: fix setup subpackage
      community/zabbix: bump pkgrel
      community/zabbix: actual build database schemas

Leo (66):
      main/alpine-baselayout: rebuild to effect renaming of locale.sh
      main/dbus: security upgrade to 1.12.18
      main/lttng-ust: split -tools to reduce installation size
      community/docker-compose: downgrade to 1.25.4
      main/gnutls: security upgrade to 3.6.14
      main/axel: add missing secfixes
      main/cups: add missing secfixes info
      main/hostapd: fix CVE-2020-12695
      community/freerdp: backport CVE fixes from 2.1.1
      community/telegram-desktop: upgrade to 2.1.11
      community/telegram-desktop: remove obsolete patches and not force gtk
      community/icinga2: fix CVE-2020-14001
      main/libjpeg-turbo: fix CVE-2020-13790
      community/freerdp: upgrade to 2.1.2
      main/ngircd: fix CVE-2020-14148
      main/libjpeg-turbo: upgrade to 2.0.5
      testing/logwatch: fix url
      community/firefox: add StartupWMClass=firefox to the .desktop entries
      main/python3: security upgrade to 3.8.4
      community/freerdp: security upgrade to 2.2.0
      main/python3: security upgrade to 3.8.5
      community/xrdp: security upgrade to 0.9.13.1
      community/pass: fix password generation
      community/libvncserver: upgrade to 0.9.13
      main/hylafaxplus: fix CVE-2020-15396 and CVE-2020-15397
      community/libvncserver: install to lib, not lib64
      main/xorg-server: fix CVE-2020-14347
      community/imagemagick: upgrade to 7.0.10.19
      community/imagemagick6: upgrade to 6.9.11.19
      community/claws-mail: security upgrade to 3.17.6
      main/patch: add missing CVE to secfixes
      main/libx11: upgrade to 1.6.11
      community/*: remove dependencies on obsolete libcroco
      community/libcroco: remove
      main/pcre: add missing secfixes
      testing/octave: disable on armhf and armv7 due to openjdk11
      main/xorg-server: upgrade to 1.20.9
      community/wireshark: security upgrade to 3.2.6
      main/ldb: upgrade to 2.1.4
      main/ldb: disable tests on ppc64le
      {main,community}/*: remove duplicate CVEs
      main/zeromq: security upgrade to 4.3.3
      community/libssh: fix CVE-2020-16135
      community/libproxy: fix CVE-2020-25219
      community/geary: fix CVE-2020-24661
      community/openexr: fix multiple CVEs
      main/curl: fix CVE-2020-8169 and CVE-2020-8177
      main/openjpeg: fix CVE-2019-12973 and CVE-2020-15389
      community/gnome-feeds: add runtime deps on py3-dateutil and py3-pillow
      main/perl-dbi: add missing secfixes info
      main/cryptsetup: fix CVE-2020-14382
      main/nss: upgrade to 3.57
      community/wireshark: security upgrade to 3.2.7
      main/ansible: upgrade to 2.9.11
      main/mbedtls: security upgrade to 2.16.8
      community/gnuchess: set a stack-size that makes gnuchess not crash
      main/oniguruma: fix CVE-2020-26159
      main/ansible: upgrade to 2.9.14
      community/libproxy: fix CVE-2020-26154
      community/pdns-recursor: security upgrade to 4.3.5
      main/libxshmfence: rebuild to fix BAD signature errors
      community/claws-mail: security upgrade to 3.17.7
      main/tzdata: upgrade to 2020c
      community/kdeconnect: security upgrade to 20.08.2
      main/freetype: security upgrade to 2.10.4
      testing/nomad: remove duplicate CVE value

Leonardo Arena (7):
      community/nextcloud: upgrade to 18.0.6
      main/smokeping: needs ttf-dejavu
      community/nextcloud: upgrade to 18.0.8
      community/nextcloud: update patch list
      community/nextcloud: upgrade to 18.0.9
      community/zabbix: upgrade to 5.0.4
      community/nextcloud: upgrade to 18.0.10

Michael Kirsch (1):
      main/knock: upgrade to 0.8.1

Michał Polański (2):
      community/prometheus: upgrade to 2.18.2
      main/nodejs: security upgrade to 12.18.4

Mike Crute (1):
      main/util-linux: fix lint errors

Milan P. Stanić (10):
      community/mutt: security upgrade to 1.14.4
      main/rng-tools: fix buffer overflow in rtlsdr entropy source
      main/postfix: upgrade to 3.5.5
      main/postfix: upgrade to 3.5.6
      main/dovecot: security upgrade to 2.3.11.3
      main/dovecot: fix check on 32bit architectures
      main/xorg-server: fix segfault on ARM machines
      main/postfix: upgrade to 3.5.7
      main/haproxy: upgrade to 2.1.8
      main/haproxy: upgrade to 2.1.9

Natanael Copa (78):
      main/gnutls: backport fixes for handling expired certs
      main/gnutls: bump pkrel
      main/ca-certificates: remove expired certificate
      main/ca-certificates: use source package from gitlab
      main/gnutls: add CVE-2020-13777 to secfixes comment
      testing/stoken: move to community
      main/zfs-lts: fix install_if for zfs-virt
      main/busybox: fix udhcpc script with multiple interfaces
      main/ca-certificates: don't delete ca-certificates.crt
      main/perl: security upgrade to 5.30.3 (CVE-2020-10543,CVE-2020-10878,CVE-2020-12723)
      community/alpine: securitu update to 2.23 (CVE-2020-14929)
      community/libraw: backport fix for CVE-2020-15503
      community/pass: add gnupg as makedepends
      main/busybox: add secfixes comment for CVE-2018-1000500
      main/libcap-ng: fix deadlock with capng_apply
      main/kamailio: add app_lua_sr to kamailio-lua
      main/freeswitch: fix ownership of /var/*/freeswitch
      main/ansible: remove duplicate in secfixes comment
      community/exim: remove dup CVE-2018-6789 in secfixes comment
      main/abuild: backport fix for unsupported byte range
      main/linux-lts: enable CONFIG_SERIAL_8250_DW
      main/linux-lts: upgrade to 5.4.47
      main/linux-lts: enable FTRACE_SYSCALLS on all arches
      main/linux-lts: upgrade to 5.4.49
      main/linux-lts: enable snd-aloop
      main/linux-lts: enable for Intel ethernet I225-LM/I225-V
      main/linux-lts: enable CGROUP_PERF for x86_64 and aarch64
      main/linux-lts: upgrade to 5.4.70
      community/jool-modules-lts: rebuild against kernel 5.4.70-r0
      community/rtl8821ce-lts: rebuild against kernel 5.4.70-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.70-r0
      community/wireguard-lts: rebuild against kernel 5.4.70-r0
      main/dahdi-linux-lts: rebuild against kernel 5.4.70-r0
      main/drbd-lts: rebuild against kernel 5.4.70-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.70-r0
      main/zfs-lts: rebuild against kernel 5.4.70-r0
      community/libetpan: backport fix for CVE-2020-15953
      main/linux-rpi: upgrade to 5.4.70
      community/jool-modules-rpi: rebuild against kernel 5.4.70-r0
      community/wireguard-rpi: rebuild against kernel 5.4.70-r0
      main/raspberrypi-bootloader: upgrade to 1.20200902
      main/linux-lts: upgrade to 5.4.71
      community/jool-modules-lts: rebuild against kernel 5.4.71-r0
      community/rtl8821ce-lts: rebuild against kernel 5.4.71-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.71-r0
      community/wireguard-lts: rebuild against kernel 5.4.71-r0
      main/dahdi-linux-lts: rebuild against kernel 5.4.71-r0
      main/drbd-lts: rebuild against kernel 5.4.71-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.71-r0
      main/zfs-lts: rebuild against kernel 5.4.71-r0
      main/linux-rpi: upgrade to 5.4.71
      community/jool-modules-rpi: rebuild against kernel 5.4.71-r0
      community/wireguard-rpi: rebuild against kernel 5.4.71-r0
      main/linux-lts: upgrade to 5.4.72
      community/jool-modules-lts: rebuild against kernel 5.4.72-r0
      community/rtl8821ce-lts: rebuild against kernel 5.4.72-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.72-r0
      community/wireguard-lts: rebuild against kernel 5.4.72-r0
      main/dahdi-linux-lts: rebuild against kernel 5.4.72-r0
      main/drbd-lts: rebuild against kernel 5.4.72-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.72-r0
      main/zfs-lts: rebuild against kernel 5.4.72-r0
      main/linux-rpi: upgrade to 5.4.72
      community/jool-modules-rpi: rebuild against kernel 5.4.72-r0
      community/wireguard-rpi: rebuild against kernel 5.4.72-r0
      community/kdeconnect: fix checksum
      community/dnsdist: disable on mips64 due to h2o
      Revert "community/kdeconnect: security upgrade to 20.08.2"
      main/nginx: fix ownership and permissions
      main/ghostscript: fix secfixes comment for CVE-2019-6116
      community/wireshark: fix secfixes comment
      main/samba: fix secfix comment for CVE-2018-14629
      main/sqlite: add CVE-2019-19244 to secfixes comment
      main/libsndfile: fix secfixes comment for CVE-2019-3832
      main/libvorbis: fix secfixes comment for CVE-2018-10393
      main/wpa_supplicant: remove CVE dupes in secfixes comment
      main/busybox: fix secfixes comment for CVE-2019-5747
      ===== release 3.12.1 =====

Niek van der Maas (1):
      community/chromium: upgrade to 83.0.4103.61

Oleg Titov (1):
      main/brotli: security upgrade to 1.0.9

Paper (1):
      community/chatty: add missing dep on cyrus-sasl-plain

PureTryOut (1):
      community/libkcddb: fix typo preventing from disabling tests

Rasmus Thomsen (121):
      community/glib-networking: security upgrade to 2.64.3
      community/gjs: upgrade to 1.64.3
      community/gnome-music: upgrade to 3.36.3
      community/evolution-data-server: upgrade to 3.36.3
      community/evolution: upgrade to 3.36.3
      community/evolution-ews: upgrade to 3.36.3
      community/nautilus: upgrade to 3.36.3
      community/gnome-system-monitor: upgrade to 3.36.1
      community/networkmanager-elogind: upgrade to 1.24.2
      community/networkmanager: upgrade to 1.24.2
      community/evince: upgrade to 3.36.3
      community/gnome-chess: upgrade to 3.36.1
      community/gnome-taquin: upgrade to 3.36.3
      community/gnome-2048: upgrade to 3.36.3
      community/librsvg: upgrade to 2.48.5
      community/epiphany: upgrade to 3.36.2
      community/orca: upgrade to 3.36.3
      community/firefox: security upgrade to 77.0
      community/librsvg: upgrade to 2.48.6
      community/chromium: disable on armv7, SEGFAULTs CLang during build
      community/corecollector: upgrade to 0.3.4
      community/cogl: upgrade to 1.22.8
      community/qemu: run pre-install as pre-upgrade hook so users are created during upgrade
      community/gnome-feeds: add missing deps: py3-gobject3 and py3-pygments
      community/librsvg: upgrade to 2.48.7
      community/evince: upgrade to 3.36.5
      community/gnome-boxes: upgrade to 3.36.5
      community/rust: upgrade to 1.44.0
      main/nss: security upgrade to 3.53.1
      community/apk-polkit: upgrade to 0.5.2
      community/chromium: security upgrade to 83.0.4103.116
      community/gnome-software-plugin-apk: depend on alpinelinux-appstream-data
      community/alpinelinux-appstream-data: upgrade to 20200628
      community/apk-polkit: upgrade to 0.5.3
      main/gtk+3.0: upgrade to 3.24.21
      community/gnome-clocks: rebuild against vala 0.48.6
      community/gtksourceview4: upgrade to 4.6.1
      main/vala: upgrade to 0.48.7
      community/gede: move from testing
      community/getting-things-gnome: move from testing
      community/py3-liblarch: move from testing
      community/hplip: move from testing
      community/vala-language-server: move from testing
      community/vala-language-server: upgrade to 0.48
      community/mozjs68: security upgrade to 68.9.0
      community/mozjs68: upgrade to 68.10.0
      community/firefox: rename desktop file to be in sync with appdata & generate appdata
      community/openrc-settingsd: fix setting /etc/localtime if its a symlink
      community/mellowplayer: upgrade to 3.6.3
      community/mellowplayer: upgrade to 3.6.4
      community/ion-shell: move from testing
      community/gnome-maps: upgrade to 3.36.4
      community/evince: upgrade to 3.36.7
      community/librsvg: upgrade to 2.48.8
      community/gnome-control-center: upgrade to 3.36.4
      community/evolution-data-server: upgrade to 3.36.4
      community/evolution: upgrade to 3.36.4
      community/evolution-ews: upgrade to 3.36.4
      community/epiphany: upgrade to 3.36.3
      main/glib: upgrade to 2.64.4
      community/alpinelinux-appstream-data: upgrade to 20200704
      community/uhttpmock: enable GIR and Vala bindings
      community/gnome-initial-setup: upgrade to 3.36.4
      community/gnome-music: upgrade to 3.36.4
      community/openrc-settingsd: ensure proper permissions on localtime and timezone
      community/firefox: upgrade to 78.0.2
      main/libinput: move quirks and udev rules to -libs subpkg
      community/webkit2gtk: upgrade to 2.28.3
      community/squeekboard: upgrade to 1.9.2
      community/libdwarf: don't install libtool script for dwarfdump binary
      community/alpinelinux-appstream-data: upgrade to 20200728
      community/firefox: security upgrade to 79.0
      community/webkit2gtk: security upgrade to 2.28.4
      community/firefox: enable WebRTC screen sharing for wayland
      community/cbindgen: upgrade to 0.14.3
      main/nspr: upgrade to 4.27
      main/nss: security upgrade to 3.55
      community/apk-polkit: upgrade to 0.6.0
      community/gnome-software-plugin-apk: upgrade to 0.7.0
      main/freeswitch: enable mod_pgsql
      main/vala: upgrade to 0.48.9
      community/gnome-desktop: upgrade to 3.36.4
      community/gnome-authenticator: fix startup with upstream patch
      community/gnome-music: upgrade to 3.36.4.1
      community/mutter: upgrade to 3.36.4
      community/gnome-shell: upgrade to 3.36.4
      community/gnome-2048: upgrade to 3.36.4
      community/gjs: upgrade to 1.64.4
      community/evolution-data-server: upgrade to 3.36.5
      community/evolution: upgrade to 3.36.5
      community/evolution-ews: upgrade to 3.36.5
      main/gtk+3.0: upgrade to 3.24.22
      community/gnome-desktop: upgrade to 3.36.5
      community/gnome-disk-utility: upgrade to 3.36.3
      community/gdm: upgrade to 3.36.3
      community/simple-scan: upgrade to 3.36.4
      community/orca: upgrade to 3.36.5
      community/alpinelinux-appstream-data: upgrade to 20200821
      main/glib: upgrade to 2.64.5
      community/gnome-flashback: upgrade to 3.36.4
      community/firefox-esr: upgrade to 78.2.0
      community/firefox: security upgrade to 80.0
      community/apk-polkit: upgrade to 0.6.1
      community/mutter: upgrade to 3.36.5
      community/gnome-shell: security upgrade to 3.36.5
      community/corecollector: upgrade to 0.3.5
      Revert "community/rclone: upgrade to 1.52.3"
      community/mutter: upgrade to 3.36.6
      community/gnome-shell: upgrade to 3.36.6
      community/gnome-desktop: upgrade to 3.36.6
      community/alpinelinux-appstream-data: upgrade to 20200910
      community/appstream: add -dbg subpkg
      community/appstream: add upstream patch to fix SEGFAULTs in appstream-generator
      main/libarchive: add -dbg subpkg
      community/firefox: upgrade to 80.0.1
      main/libvirt: add -dbg subpackage
      community/firefox: upgrade to 81.0
      community/gnome-bluetooth: upgrade to 3.34.2
      community/polari: add missing dependencies
      main/glib: upgrade to 2.64.6
      main/vala: upgrade to 0.48.11

Simon Frankenberger (4):
      community/docker-compose: upgrade to 1.26.0
      community/py3-dotenv: new aport
      community/bareos: security upgrade to 18.2.9
      community/openjdk12: upgrade to 11.0.8

Sora Morimoto (1):
      community/opam: upgrade to 2.0.7

Sören Tempel (2):
      community/firefox-esr: upgrade to 68.9.0
      main/groff: prevent conflict with mandoc-doc

Thomas Liske (10):
      community/smstools: use checkpath in initd to create non-public files
      community/anytun: use checkpath in initd to create non-public files
      community/postsrsd: use checkpath in initd to create non-public files
      main/postgresql-bdr: use checkpath in initd to create non-public files
      main/clamav: use checkpath in initd to create non-public files
      main/openrc: use checkpath in initd to create non-public files
      main/iptables: fix ebtables save
      main/iptables: drop broute table name
      main/iptables: use checkpath in initd to create non-public files
      main/linux-firmware: add amd-ucode subpackage

Tim Brust (1):
      main/nghttp2: security upgrade to 1.41.0

Timo Teräs (1):
      main/musl: fix serious missing synchronization bug

Trung Le (1):
      main/linux-lts: Enable CONFIG_CGROUP_PIDS for ppc64le

Trung Lê (2):
      [3.12] community/nodejs-current: Bump to 14.5.0
      main/nodejs: upgrade to 12.18.3

William Johansson (1):
      main/linux-lts: enable CONFIG_XEN_WDT=m on linux-virt

prspkt (10):
      community/ffmpeg: upgrade to 4.3
      community/twm: upgrade to 1.0.11
      main/dropbear: backport security fixes
      community/znc: upgrade to 1.8.1
      community/ffmpeg: security upgrade to 4.3.1
      main/libx11: security upgrade to 1.6.10
      main/chrony: security upgrade to 3.5.1
      main/libx11: security upgrade to 1.6.12
      main/putty: upgrade to 0.74
      main/gnupg: upgrade to 2.2.21

wener (2):
      community/rclone: upgrade to 1.52.3
      community/stress-ng: upgrade to 0.11.21

</pre>
