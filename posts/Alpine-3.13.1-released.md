---
title: 'Alpine 3.13.1 released'
date: 2021-01-28
---

Alpine Linux 3.13.1 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.13.1 of its Alpine Linux operating system.

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.13.1).

Git Shortlog
------------

<pre>
Andy Postnikov (5):
      community/drupal7: security upgrade to 7.78 - CVE-2020-36193
      community/php7-pecl-timezonedb: upgrade to 2021.1
      community/php8-pecl-timezonedb: upgrade to 2021.1
      main/tzdata: upgrade to 2021a
      community/composer: upgrade to 2.0.9

Ariadne Conill (2):
      main/musl: upgrade to 1.2.2
      main/bonding: add support for bond-members statements

Bart Ribbers (4):
      community/plasma-desktop: add missing dep on accountsservice
      community/plasma: add dep on kio-fuse as requested upstream
      community/okular: add missing recommended deps
      community/kwin: build with pipewire support

Craig Comstock (1):
      community/cfengine: fix libntech configure

Daniel Néri (2):
      main/xen: fix XSA-360
      main/nsd: upgrade to 4.3.5

Dermot Bradley (3):
      community/s6-overlay-preinit: rebuild for skalibs ABI change
      community/justc-envdir: fix build with new skalibs
      community/cloud-init: upgrade to 20.4.1

Drew DeVault (1):
      main/doas: patch out PATH reset vulnerability

Francesco Colista (8):
      community/py3-multidict-gns3: new aport as dependency of gns3
      community/py3-yarl-gns3: new dependency of gns3
      community/py3-chardet-gns3: new aport as dependency of gns3
      community/py3-aiohttp-gns3: added new dependency
      community/gns3-server: bump pkgrel for #12334 fix
      community/py3-aiohttp-gns3: bump pkgrel to get new deps
      testing/perl-data-validate-ip: new aport
      testing/ddclient: new aport

Henrik Riomar (1):
      main/xen: add missing CVE info

J0WI (1):
      community/vlc: security upgrade to 3.0.12

Jakub Jirutka (4):
      community/kea: fix install location of kea-admin scripts
      main/esh: upgrade to 0.3.1
      main/postgresql: fix check_* functions in init script
      main/postgresql: add init commands stop_fast, stop_force, stop_smart, reload_force

James Stone (1):
      community/x2goserver: add missing dep on perl-try-tiny

Kevin Daudt (5):
      main/m4: fix realpath test
      community/zabbix: add missing webif dependencies
      community/zabbix: upgrade to 5.2.4
      main/sudo: security upgrade to 1.9.5p2 (CVE-2021-3156)
      main/doas: include secfixes for CVE-2019-25016

Leo (69):
      testing/py3-rich: new aport
      community/py3-rich: move from testing
      community/ansible-lint: add missing dependecy on py3-rich
      community/py3-fs: upgrade to 2.4.12
      testing/py3-intervals: upgrade to 0.9.1
      main/btrfs-progs: upgrade to 5.10
      community/py3-tox: upgrade to 3.21.2
      main/py3-tz: upgrade to 2020.5
      community/py3-authlib: upgrade to 0.15.3
      main/py3-yaml: fix CVE-2020-14343
      community/gkraken: upgrade to 0.14.5
      main/sqlite: upgrade to 3.34.1
      main/sqlite-tcl: upgrade to 3.34.1
      community/py3-prompt_toolkit: upgrade to 3.0.11
      community/py3-bleach: upgrade to 3.2.2
      testing/py3-fastavro: upgrade to 1.2.4
      main/monit: upgrade to 5.27.2
      community/py3-unidecode: upgrade to 1.1.2
      community/py3-typed-ast: upgrade to 1.4.2
      main/py3-setuptools: upgrade to 51.3.3
      community/py3-s3transfer: upgrade to 0.3.4
      main/py3-coverage: upgrade to 5.3.1
      community/py3-rsa: upgrade to 4.7
      main/bind: upgrade to 9.16.11
      community/py3-breathe: upgrade to 4.26.1
      community/py3-freezegun: upgrade to 1.1.0
      community/mutt: fix CVE-2021-3181
      testing/deutex: upgrade to 5.2.2
      main/doxygen: upgrade to 1.9.1
      main/umurmur: upgrade to 0.2.18
      main/razor: fix license, manpage installation
      testing/rlwrap: upgrade to 0.44
      community/flatpak: upgrade to 1.10.1
      testing/s3fs-fuse: upgrade to 1.88
      community/py3-pytest-cov: upgrade to 2.11.1
      testing/coredns: upgrade to 1.8.1
      main/pango: upgrade to 1.48.1
      main/py3-pillow: fix CVE-2020-35655
      community/py3-prompt_toolkit: upgrade to 3.0.12
      community/py3-prompt_toolkit: upgrade to 3.0.13
      main/pinentry: upgrade to 1.1.1
      community/py3-saml2: fix CVE-2021-21239
      community/py3-prompt_toolkit: upgrade to 3.0.14
      main/libxt: upgrade to 1.2.1
      community/gnote: upgrade to 3.38.1
      community/pipewire: upgrade to 0.3.20
      community/xournalpp: upgrade to 1.0.20
      community/vte3: upgrade to 0.62.2
      community/gpaste: upgrade to 3.38.4
      community/gtk4.0: upgrade to 4.0.2
      community/mutter: upgrade to 3.38.3
      community/enchant2: upgrade to 2.2.15
      community/gnome-shell: upgrade to 3.38.3
      community/py3-pip: upgrade to 20.3.4
      community/flatpak: add missing secfixes info
      community/ccid: upgrade to 1.4.34
      main/perl-datetime-timezone: upgrade to 2.47
      community/feh: upgrade to 3.6.3
      testing/libspatialite: fix source=
      community/openvswitch: upgrade to 2.12.2
      community/py3-bleach: upgrade to 3.2.3
      community/coturn: security upgrade to 4.5.2
      community/telepathy-glib: upgrade to 0.24.2
      testing/googler: upgrade to 4.3.2
      testing/dnstwist: upgrade to 20201228
      community/py3-tox: upgrade to 3.21.3
      testing/py3-rply: upgrade to 0.7.8
      main/ansible: upgrade to 2.10.6
      main/privoxy: upgrade to 3.0.30

Leonardo Arena (2):
      community/nextcloud19: upgrade to 19.0.7
      community/nextcloud19: bump for checksums

Michał Polański (1):
      community/go: security upgrade to 1.15.7

Milan P. Stanić (7):
      main/gptfdisk: security upgrade to 1.0.6
      main/dnsmasq: security upgrade to 2.83
      main/postfix: upgrade to 3.5.9
      community/mutt: security upgrade to 2.0.5
      community/fsverity-utils: upgrade to 1.3
      community/libudev-zero: move from testing
      main/dnsmasq: upgrade to 2.84

Natanael Copa (25):
      main/util-linux: unbreak user mounts
      main/busybox: fix echo write error: Invalid argument
      main/busybox: update echo fix
      main/linux-lts: upgrade to 5.10.9
      main/linux-lts: upgrade to 5.10.10
      community/jool-modules-lts: rebuild against kernel 5.10.10-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.10-r0
      community/rtpengine-lts: rebuild against kernel 5.10.10-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.10-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.10-r0
      main/zfs-lts: rebuild against kernel 5.10.10-r0
      main/linux-rpi: upgrade to 5.10.10
      community/jool-modules-rpi: rebuild against kernel 5.10.10-r0
      main/zfs-rpi: rebuild against kernel 5.10.10-r0
      main/linux-rpi: upgrade to 5.10.11
      community/jool-modules-rpi: rebuild against kernel 5.10.11-r0
      main/zfs-rpi: rebuild against kernel 5.10.11-r0
      main/linux-lts: upgrade to 5.10.11
      community/jool-modules-lts: rebuild against kernel 5.10.11-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.11-r0
      community/rtpengine-lts: rebuild against kernel 5.10.11-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.11-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.11-r0
      main/zfs-lts: rebuild against kernel 5.10.11-r0
      ===== release 3.13.1 =====

Rasmus Thomsen (3):
      community/glibd: rebuild against ldc
      community/appstream: add -compose subpkg
      community/gnome-podcasts: add missing dep on gst-plugins-good

TBK (1):
      community/libudev-zero: upgrade to 0.4.7

Timo Teräs (1):
      main/apk-tools: upgrade to 2.12.1

Veovis (1):
      main/linux-lts: enable CIFS_UPCALL on all arches

wener (2):
      community/grpc: upgrade to 1.34.1
      community/k3s: upgrade to 1.20.2

</pre>
