---
title: 'AlpineConf 2021'
date: 2021-05-13
---

# AlpineConf 2021

We are pleased to announce AlpineConf 2021, which will be held the weekend of
15th and 16th of May.

See [the schedule](/conf/) for more details.

